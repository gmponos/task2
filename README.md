# Task2

## Notes
- In the parent folder there is a MySQL workbench model included with the schema of the DB.

## About the code.
- Since the task was saying _how you approach OO design problems_ I focused more on trying to keep decoupled the 
View layer from the database layer rather focusing on functionality of the project. Most of the times applications 
are using their Entities directly inside the views or in serializers and stuff.

You can see in places like [this](https://bitbucket.org/gmponos/task2/src/557bdc72c2e8bd1b456eed1109892b07b52eb1d5/src/AppBundle/Form/?at=master) that i tried
to create classes that represent the view layer and not coupling them with the Entities of doctrine.

This effort cost me some more time.

- Also I like to have my controller separated and as thin as possible. That is why I created a subfolder named [API](https://bitbucket.org/gmponos/task2/src/557bdc72c2e8bd1b456eed1109892b07b52eb1d5/src/AppBundle/Controller/Api/?at=master)
The intention was to have all the needed APIs there.