<?php

namespace AppBundle\Form;

use Symfony\Component\Validator\Constraints as Assert;

class Message
{
    /**
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @Assert\NotBlank()
     */
    private $chatroom;

    /**
     * Message constructor.
     *
     * @param $content
     * @param $user
     * @param $chatroom
     */
    public function __construct($content, $user, $chatroom = null)
    {
        $this->content = $content;
        $this->user = $user;
        $this->chatroom = $chatroom;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getChatroom()
    {
        return $this->chatroom;
    }
}