<?php

namespace AppBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Chatroom
{
    /**
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Assert\Count(
     *      min = 2,
     *      minMessage = "You must select at least two users",
     * )
     */
    private $users;

    public function __construct($name, $users)
    {
        $this->name = $name;
        $this->users = $users;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }
}