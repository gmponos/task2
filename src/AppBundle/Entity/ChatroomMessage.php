<?php

namespace AppBundle\Entity;

class ChatroomMessage
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Chatroom
     */
    private $chatroom;

    /**
     * @var Message
     */
    private $message;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chatroom
     *
     * @param Chatroom $chatroom
     *
     * @return ChatroomMessage
     */
    public function setChatroom(Chatroom $chatroom)
    {
        $this->chatroom = $chatroom;

        return $this;
    }

    /**
     * Get chatroom
     *
     * @return Chatroom
     */
    public function getChatroom()
    {
        return $this->chatroom;
    }

    /**
     * Set messages
     *
     * @param Message $messages
     *
     * @return ChatroomMessage
     */
    public function setMessage(Message $messages)
    {
        $this->message = $messages;

        return $this;
    }

    /**
     * Get messages
     *
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }
}

