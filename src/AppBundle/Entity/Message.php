<?php

namespace AppBundle\Entity;

class Message
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var \DateTimeInterface
     */
    private $sent;

    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    public function __construct()
    {
        $this->sent = new \DateTimeImmutable();
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sent
     *
     * @param \DateTimeInterface $sent
     *
     * @return Message
     */
    public function setSent(\DateTimeInterface $sent)
    {
        $this->sent = $sent;
        return $this;
    }

    /**
     * Get sent
     *
     * @return \DateTimeInterface
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Message
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}

