<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Chatroom;
use AppBundle\Entity\User;

class ChatroomUser
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Chatroom
     */
    private $chatroom;

    /**
     * @var User
     */
    private $user;

    /**
     * Set id
     *
     * @param int $id
     *
     * @return ChatroomUser
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chatroom
     *
     * @param Chatroom $chatroom
     *
     * @return ChatroomUser
     */
    public function setChatroom(Chatroom $chatroom)
    {
        $this->chatroom = $chatroom;

        return $this;
    }

    /**
     * Get chatroom
     *
     * @return Chatroom
     */
    public function getChatroom()
    {
        return $this->chatroom;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return ChatroomUser
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}

