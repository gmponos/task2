<?php

namespace AppBundle\Entity;

class SystemMessage
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Message
     */
    private $message;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messages
     *
     * @param \AppBundle\Entity\Message $message
     * @return SystemMessage
     */
    public function setMessage(Message $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get messages
     *
     * @return \AppBundle\Entity\Message
     */
    public function getMessage()
    {
        return $this->message;
    }
}

