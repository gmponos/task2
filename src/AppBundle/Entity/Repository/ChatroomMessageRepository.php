<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ChatroomMessage;
use Doctrine\ORM\EntityRepository;

/**
 * @method ChatroomMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChatroomMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChatroomMessage[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method ChatroomMessage[] findAll()
 */
class ChatroomMessageRepository extends EntityRepository
{
    /**
     * @param ChatroomMessage $chatroomMessage
     */
    public function save(ChatroomMessage $chatroomMessage)
    {
        $em = $this->getEntityManager();
        $em->persist($chatroomMessage);
        $em->flush();
    }
}