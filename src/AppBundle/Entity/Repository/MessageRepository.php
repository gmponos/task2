<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Message;
use AppBundle\Entity\SystemMessage;
use Doctrine\ORM\EntityRepository;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Message[] findAll()
 */
class MessageRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\SystemMessage $systemMessage
     */
    public function save(SystemMessage $systemMessage)
    {
        $em = $this->getEntityManager();
        $em->persist($systemMessage);
        $em->flush();
    }

    /**
     * The goal of this method is to find all message by joining the system messages and the chat messages
     * all together.
     *
     * @param int $chatroomId
     * @return Message[]
     */
    public function findAllMessagesToDisplay($chatroomId)
    {
        return $this->createQueryBuilder('m')
            ->select()
            ->leftJoin('AppBundle:SystemMessage', 'sm', 'WITH', 'sm.message = m.id')
            ->join('AppBundle:ChatroomMessage', 'cm', 'WITH', 'cm.message = m.id')
//            ->setParameter('chatroomId', $chatroomId)
            ->getQuery()
            ->getResult();
    }
}