<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Chatroom;
use Doctrine\ORM\EntityRepository;

/**
 * @method Chatroom|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chatroom|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chatroom[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Chatroom[] findAll()
 */
class ChatroomRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\Chatroom $chatrooms
     */
    public function save(Chatroom $chatrooms)
    {
        $em = $this->getEntityManager();
        $em->persist($chatrooms);
        $em->flush();
    }
}