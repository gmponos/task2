<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\SystemMessage;
use Doctrine\ORM\EntityRepository;

/**
 * @method SystemMessage[] findAll()
 */
class SystemMessageRepository extends EntityRepository
{
    /**
     * @param \AppBundle\Entity\SystemMessage $systemMessage
     */
    public function save(SystemMessage $systemMessage)
    {
        $em = $this->getEntityManager();
        $em->persist($systemMessage);
        $em->flush();
    }
}