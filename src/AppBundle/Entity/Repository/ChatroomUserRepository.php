<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\ChatroomUser;
use Doctrine\ORM\EntityRepository;

/**
 * Class ChatroomUserRepository
 *
 * @method ChatroomUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChatroomUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChatroomUser[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method ChatroomUser[] findAll()
 */
class ChatroomUserRepository extends EntityRepository
{
    /**
     * @param ChatroomUser $chatroomUser
     */
    public function save(ChatroomUser $chatroomUser)
    {
        $em = $this->getEntityManager();
        $em->persist($chatroomUser);
        $em->flush();
    }
}