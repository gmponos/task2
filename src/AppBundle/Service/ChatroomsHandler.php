<?php

namespace AppBundle\Service;

use AppBundle\Entity\ChatroomUser;
use AppBundle\Entity\Repository\ChatroomRepository;
use AppBundle\Entity\Repository\ChatroomUserRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\Form\Chatroom;

class ChatroomsHandler
{
    /**
     * @var ChatroomRepository
     */
    private $chatroomRepository;

    /**
     * @var ChatroomUserRepository
     */
    private $chatroomUserRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        ChatroomRepository $chatroomRepository,
        ChatroomUserRepository $chatroomUserRepository,
        UserRepository $userRepository
    ) {
        $this->chatroomRepository = $chatroomRepository;
        $this->chatroomUserRepository = $chatroomUserRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Chatroom $chatroom
     */
    public function create(Chatroom $chatroom)
    {
        $chatroomEntity = new \AppBundle\Entity\Chatroom();

        $chatroomEntity->setName($chatroom->getName());
        $this->chatroomRepository->save($chatroomEntity);

        $chatroom->getUsers()->forAll(function ($key, User $user) use ($chatroomEntity) {
            $userEntity = $this->userRepository->find($user->getId());
            if ($userEntity === null) {
                throw new \UnexpectedValueException();
            }

            $chatroomUser = new ChatroomUser();
            $chatroomUser->setUser($userEntity)->setChatroom($chatroomEntity);
            $this->chatroomUserRepository->save($chatroomUser);
            return true;
        });
    }
}