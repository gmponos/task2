<?php

namespace AppBundle\Service;

use AppBundle\Entity\Chatroom;
use AppBundle\Entity\ChatroomMessage;
use AppBundle\Entity\Message;
use AppBundle\Entity\Repository\ChatroomMessageRepository;
use AppBundle\Entity\Repository\ChatroomRepository;
use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Form\Message as FormMessage;

class MessageHandler
{
    /**
     * @var ChatroomRepository
     */
    private $chatroomRepository;

    /**
     * @var ChatroomMessageRepository
     */
    private $chatroomMessageRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        ChatroomRepository $chatroomRepository,
        ChatroomMessageRepository $chatroomMessageRepository,
        UserRepository $userRepository
    ) {
        $this->chatroomRepository = $chatroomRepository;
        $this->chatroomMessageRepository = $chatroomMessageRepository;
        $this->userRepository = $userRepository;
    }

    public function createMessage(FormMessage $message)
    {
        $entityUser = $this->userRepository->find($message->getUser());
        $entityMessage = new Message();
        $entityMessage->setUser($entityUser);
        $entityMessage->setContent($message->getContent());

        if ($entityUser->getRole()->getRole() === 'admin') {
            $this->createAsSystemMessage($entityMessage, $entityUser);
        } else {
            $chatroom = $this->chatroomRepository->find($message->getChatroom());
            if($chatroom === null){
                throw new \UnexpectedValueException($message->getChatroom());
            }
            $this->createAsChatroomMessage($entityMessage, $chatroom);
        }
    }

    private function createAsSystemMessage($entityMessage, $entityUser)
    {
    }

    private function createAsChatroomMessage(Message $message, Chatroom $chatroom)
    {
        $chatroomMessage = new ChatroomMessage();
        $chatroomMessage->setChatroom($chatroom);
        $chatroomMessage->setMessage($message);
        $this->chatroomMessageRepository->save($chatroomMessage);
    }
}