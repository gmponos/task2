<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Role controller.
 *
 * @Route("roles")
 */
class RolesController extends Controller
{
    /**
     * Lists all role entities.
     *
     * @Route("/", name="role_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $roles = $this->getDoctrine()->getRepository(Role::class)->findAll();
        return $this->render('roles/index.html.twig', [
            'roles' => $roles,
        ]);
    }
}
