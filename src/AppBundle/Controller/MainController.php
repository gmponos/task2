<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Message controller.
 *
 * @Route("/")
 */
class MainController extends Controller
{
    /**
     * Lists all message entities.
     *
     * @Route("/", name="main_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('main.index.html.twig');
    }
}