<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Chatroom;
use AppBundle\Form\ChatroomType;
use AppBundle\Service\ChatroomsHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Chatroom controller.
 *
 * @Route("chatrooms")
 */
class ChatroomsController extends Controller
{
    /**
     * Lists all chatrooms.
     *
     * @Route("/", name="chatrooms_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $chatrooms = $this->getDoctrine()->getRepository(Chatroom::class)->findAll();
        return $this->render('chatrooms/index.html.twig', [
            'chatrooms' => $chatrooms,
        ]);
    }

    /**
     * Creates a new chatroom.
     *
     * @Route("/new", name="chatrooms_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(ChatroomType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get(ChatroomsHandler::class)->create($form->getData());
            return $this->redirectToRoute('chatrooms_index');
        }

        return $this->render('chatrooms/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a chatroom entity.
     *
     * @Route("/view/{id}", name="chatrooms_view")
     * @Method("GET")
     * @param Chatroom $chatroom
     * @return Response
     */
    public function viewAction(Chatroom $chatroom)
    {
        return $this->render('chatrooms/view.html.twig', [
            'chatroom' => $chatroom,
        ]);
    }
}
