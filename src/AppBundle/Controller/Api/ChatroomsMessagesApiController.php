<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ChatroomsApiController
 *
 * @Route("/api/chatrooms_messages")
 */
class ChatroomsMessagesApiController extends Controller
{
    /**
     * @Route("/{chatroomId}", name="chatrooms_messages_api_list")
     * @Method("GET")
     * @param int $chatroomId
     * @return JsonResponse
     */
    public function listAction($chatroomId)
    {
        $chatroomMessages = $this
            ->getDoctrine()
            ->getRepository(Message::class)
            ->findAllMessagesToDisplay($chatroomId);

        $data['data'] = [];
        if (empty($chatroomMessages)) {
            return $this->json($data);
        }

        foreach ($chatroomMessages as $chatroomMessage) {
            $data['data'][] = [
                'content' => $chatroomMessage->getContent(),
                'user' => $chatroomMessage->getUser()->getUsername(),
                'sent' => $chatroomMessage->getSent()->format('Y-m-d H:i:s'),
            ];
        }

        return $this->json($data);
    }
}
