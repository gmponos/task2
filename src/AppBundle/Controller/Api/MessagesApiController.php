<?php

namespace AppBundle\Controller\Api;

use AppBundle\Form\Message;
use AppBundle\Service\MessageHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ChatroomsApiController
 *
 * @Route("/api/messages")
 */
class MessagesApiController extends Controller
{
    /**
     * @Route("/new", name="messages_api_new")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function newAction(Request $request)
    {
        parse_str($request->getContent(), $output);
        /** @var Message $message */
        $message = $this->get('serializer')->denormalize($output, Message::class);
        $errors = $this->get('validator')->validate($message);

        if ($errors->count() > 0) {
            return new JsonResponse(['validation errors'], 400);
        }

        $this->get(MessageHandler::class)->createMessage($message);
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
