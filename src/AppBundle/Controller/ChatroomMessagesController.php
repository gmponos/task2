<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Message controller.
 *
 * @Route("chatroom_messages")
 */
class ChatroomMessagesController extends Controller
{
    /**
     * Finds and displays a message entity.
     *
     * @Route("/enter/{id}", name="chatroom_messages_enter")
     * @Method("GET")
     * @param int $id
     * @return Response
     */
    public function enterAction($id)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findBy([
            'role' => 1,
        ]);
        return $this->render('chatroom_messages/enter.html.twig', [
            'users' => $users,
            'chatroom_id' => $id,
        ]);
    }
}
