<?php

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class FormLoginGuardAuthenticator extends AbstractGuardAuthenticator
{
    private $router;

    private $encoder;

    public function __construct(UrlGeneratorInterface $router, UserPasswordEncoderInterface $encoder)
    {
        $this->router = $router;
        $this->encoder = $encoder;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('app_login'));
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        if (!$request->isMethod(Request::METHOD_POST)) {
            return null;
        }

        if ('app_login_check' !== $request->attributes->get('_route')) {
            return null;
        }

        $params = $request->request->all();
        if (empty($params['my_login']) || empty($params['password'])) {
            throw new CustomUserMessageAuthenticationException('security.authentication.missing_credentials');
        }

        return [
            'username' => $params['login'],
            'password' => $params['password'],
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            return $userProvider->loadUserByUsername($credentials['username']);
        } catch (\Exception $e) {
            throw new CustomUserMessageAuthenticationException(
                'security.authentication.username_not_exists',
                ['%username%' => $credentials['username']],
                0,
                $e
            );
        }
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->encoder->isPasswordValid($user, $credentials['password']);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return $this->start($request, $exception);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->router->generate('main_index'));
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
